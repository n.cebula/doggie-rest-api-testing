describe('By sub-breed endpoint', () => {
    beforeEach(() => {
        cy.request('/breed/hound/list').as('hound')
    });

    it('Validate the header', () => {
        cy.get('@hound')
            .its('headers')
            .its('content-type')
            .should('include', 'application/json');
    });

    it('Validate the status code', () => {
        cy.get('@hound')
            .its('status')
            .should('equal', 200);
    });

    it('Validate that list of sub-breeds is not empty', () => {
        cy.get('@hound').its('body').then((body) => {
            expect(body.message).to.not.be.empty
        })
    });

    it('Validate the number of sub-breeds', () => {
        cy.get('@hound').its('body').then((body) => {
            expect(body.message).to.have.length(7)
        })
    });

});