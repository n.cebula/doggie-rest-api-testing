describe('List all breeds endpoint', () => {
    beforeEach(() => {
        cy.request('/breeds/list/all').as('breeds')
    });

    it('Validate the header', () => {
        cy.get('@breeds')
            .its('headers')
            .its('content-type')
            .should('include', 'application/json');
    });

    it('Validate the status code', () => {
        cy.get('@breeds')
            .its('status')
            .should('equal', 200);
    });

    it('Validate the dog\'s breed', () => {
        cy.get('@breeds').its('body').then((body) => {
            expect(body.message).to.have.nested.property('corgi')
        });
    });

    it('Validate the above dog\'s sub-breed', () => {
        cy.get('@breeds').its('body').then((body) => {
            expect(body.message.corgi).to.include('cardigan')
        });
    });

});