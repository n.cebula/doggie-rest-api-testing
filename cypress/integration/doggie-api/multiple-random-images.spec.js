describe('Multiple random dog images by breed endpoint', () => {
    beforeEach(() => {
        cy.request('/breed/corgi/images/random/5').as('corgi')
    });

    it('Validate the header', () => {
        cy.get('@corgi')
            .its('headers')
            .its('content-type')
            .should('include', 'application/json');
    });

    it('Validate the status code', () => {
        cy.get('@corgi')
            .its('status')
            .should('equal', 200);
    });

    it('Validate the number of random images', () => {
        cy.get('@corgi').its('body').then((body) => {
            expect(body.message).to.have.length(5)
        })
    });

    it('Validate that listed images are correctly assigned to the breed', () => {
        cy.get('@corgi').
        its('body').
        its('message').then((message) => {
            message.forEach((key) => {
                expect(key).to.include(['corgi'])
            });
        });
    });

});