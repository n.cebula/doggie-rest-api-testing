describe('By breed endpoint', () => {
    beforeEach(() => {
        cy.request('/breed/corgi/images').as('corgi')
    });

    it('Validate the header', () => {
        cy.get('@corgi')
            .its('headers')
            .its('content-type')
            .should('include', 'application/json');
    });

    it('Validate the status code', () => {
        cy.get('@corgi')
            .its('status')
            .should('equal', 200);
    });

    it('Validate the number of images', () => {
        cy.get('@corgi').its('body').then((body) => {
            expect(body.message).to.have.length(164)
        })
    });

    it('Validate that listed images are correctly assigned to the breed', () => {
        cy.get('@corgi').
        its('body').
        its('message').then((message) => {
            message.forEach((key) => {
                expect(key).to.include(['corgi'])
            });
        });
    });

    /* THIS WORKS SIMILARLY TO THE ABOVE ONE BUT IT'S NOTICEABLY SLOWER SO I'LL JUST LEAVE IT HERE :)
        it('Validate that listed images are correctly assigned to the breed', () => {
            cy.get('@corgi')
                .its('body')
                .its('message')
                .each((key) => {
                    cy.wrap(key)
                        .should('include', 'corgi')
                });
        });*/

});